# Ansible Role: Common
Installs and configures common packages, which are necessary to be on all servers.

## Requirements

This role require Ansible 2.9 or higher.

This role was designed for:
  - AlmaLinux OS 9
  
## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    common_packages: []

A list of the comman packages to install, like `nano`, `htop`, `unzip`, `ncdu`, `patch` and etc.

    common_packages_extra: []
    
A list of extra common packages to install without overriding the default list.

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Maksim Soldatjonok](https://www.maksold.com/).